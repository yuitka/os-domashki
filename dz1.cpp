#include "stdafx.h"
#include <list>
#include <iostream>
#include <conio.h>
#include <time.h>
#include <string>
#include <vector>
using namespace std;

int main()
{
	setlocale(LC_ALL, "Russian");
	srand(time(NULL));
	int arrsize = 1+rand()%10; // динам. массив случайной длины от 1 до 11, можно в принципе любой другой длины > 1
	int *mas = new int[arrsize];
	cout << "Первый случайный массив: ";
	for (int i = 0; i < arrsize; i++)
	{
		//mas[i] = rand()%65000; // в задании были элементы от 0 до 65000
		mas[i] = rand() % 20;
		// для проверки удобны числа поменьше
		cout << mas[i] << " ";
	}
	cout << endl;
	
	int temp = mas[0];
	for (int i = 0; i < arrsize; i++)
	{
		if (mas[i] >= temp)
			temp = mas[i];
	}
	cout << "Максимальный элемент: " << temp << endl;

	cout << "Второй случайный массив: ";
	int *mas2 = new int[temp];
	int numb = 1;
	for (int i = 0; i < temp; i++)
	{
		mas2[i] = numb;
		numb++;
		cout << mas2[i] << " ";
	}
	cout << endl;

	
	cout << "Элементы, которых не хватает в первом массиве: " << endl;
	int temp2;
	bool switch1; // индикатор того, встретился ли элемент в первом массиве
	for (int i = 0; i < temp; i++)
	{
		temp2 = mas2[i];
		switch1 = 0;
		for (int j = 0; j < arrsize; j++)
		{
			if (mas[j] == mas2[i])
			{
					switch1 = 1;
			}
		}
		if(switch1==0) // если элемент не встретился, выводим его на экран
			cout << mas2[i] << " ";
	}
	cout << endl;

	getch();
	return 0;
}